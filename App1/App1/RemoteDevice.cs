﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App1
{
    public class RemoteDevice
    {
        public string name;
        public string mac;

        public RemoteDevice(string name0, string mac0) {
            name = name0;
            mac = mac0;
        }
    }
}
