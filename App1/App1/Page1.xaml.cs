﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Timers;

//Timer, który wysyła każde 0.1 sekundy

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        
        List<RemoteDevice> devices;
        ObservableCollection<string> devicesNames;
        bool isSearching;
        string address;

		public Page1 ()
		{
			InitializeComponent ();
            devices = new List<RemoteDevice>();
            devicesNames = new ObservableCollection<string>();
            address = "";
            
            isSearching = false;
        }

  

        void subscribe() {
            MessagingCenter.Subscribe<IDevice, string>(this, "android error", (sender, arg) => {
                DisplayAlert("Alert", Convert.ToString(arg), "OK");
            });
        }

        void unsubcribe() {
            MessagingCenter.Unsubscribe<IDevice, string>(this, "android error");
        }

     

        public void checkButton_Clicked(EventArgs e) {
    
           
            nameLabel.Text = DependencyService.Get<IDevice>().device_name();
            MessagingCenter.Unsubscribe<IDevice, string>(this, "android error");
          
        }

        public void showButton_Clicked(EventArgs e) {

            if (!isSearching)
            {
                Console.WriteLine("We are starting");
                devices = new List<RemoteDevice>();
                devicesNames = new ObservableCollection<string>();
                listBox.ItemsSource = devicesNames;

                //subscribe
                MessagingCenter.Subscribe<IDummy, RemoteDevice>(this, "blueDeviceFound", (sender, arg) =>
                {
                    
                    Console.WriteLine("I have got a device");
                    devicesNames.Add(arg.name);
                    devices.Add(arg);
                });
                subscribe();
                //end subscribe

                DependencyService.Get<IDevice>().GetBlueList();

                isSearching = true;
                showButton.Text = "Cancel discovery";

            }
            else {
                MessagingCenter.Send<Page1>(this, "stop discovery");
                DisplayAlert("Alert", "Discovery is stopped", "OK");

                //unsubcribe
                MessagingCenter.Unsubscribe<IDummy, RemoteDevice>(this, "blueDeviceFound");
                unsubcribe();
                isSearching = false;
                showButton.Text = "Show devices";
            }

        }

        private void listBox_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
           address = devices[devicesNames.IndexOf((string)e.SelectedItem)].mac;
            
        }

   

        private void layoutButton1_Clicked(object sender, EventArgs e)
        {
            joyLayout.IsVisible = false;
            blueLayout.IsVisible = true;
            layoutButton1.BorderWidth = 0;
            layoutButton2.BorderWidth = 2;
        }

        private void layoutButton2_Clicked(object sender, EventArgs e)
        {
            blueLayout.IsVisible = false;
            joyLayout.IsVisible = true;
            layoutButton1.BorderWidth = 2;
            layoutButton2.BorderWidth = 0;
        }

        private void runButton_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send<Page1>(this, "run");
        }

        private void stopButton_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send<Page1>(this, "stop");
        }

        private void connectButton_Clicked(object sender, EventArgs e)
        {
            if (address != "")
            {
                MessagingCenter.Subscribe<IDevice>(this, "connected", (sender1)=> {

                    Timer timer = new Timer(100);
                    timer.AutoReset = true;
                    timer.Elapsed += timerEvent;
                    timer.Start();
                });
                DependencyService.Get<IDevice>().connect(address);

            }
        }

        void timerEvent(Object source, ElapsedEventArgs e) {
            MessagingCenter.Send<Page1, Point>(this, "xy", new Point(-JoystickControl.Yposition, JoystickControl.Xposition));
        }
    }
}