﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Bluetooth;
using Xamarin.Forms;
using App1;

namespace App1.Droid
{

    [BroadcastReceiver(Enabled = true, Exported = true)]
    public class BlueListReceiver: BroadcastReceiver, IDummy
    {
      
        public BlueListReceiver() {
           
        }

        public override void OnReceive(Context context, Intent intent)
        {
            string name = intent.GetStringExtra(BluetoothDevice.ExtraName);
            BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
            
            string address = device.Address;
           bool valid  = BluetoothAdapter.CheckBluetoothAddress(address);
            RemoteDevice passdevice = new RemoteDevice(name, address);
            MessagingCenter.Send<IDummy, RemoteDevice>(this, "blueDeviceFound", passdevice);
            Console.WriteLine("BlueDeviceFound: " + name + " address: " + address + " is valid: " + Convert.ToString(valid));
        }

    }
}