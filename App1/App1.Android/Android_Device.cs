﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App1;
using Android.Bluetooth;
using Xamarin.Forms;
using System.Threading.Tasks;
using tm = System.Timers;
using Java.Util;
using System.IO;


[assembly: Xamarin.Forms.Dependency(typeof(App1.Droid.Device_Android))]
namespace App1.Droid
{
    class Device_Android : IDevice
    {

        
        BluetoothAdapter bluetooth;
        static string uudi = "00001101-0000-1000-8000-00805F9B34FB";

        public Device_Android()
        {
            
        }

        //Sprawdza bluetooth
        public string device_name()
        {
            BluetoothAdapter bluetooth = bluetoothCheck();
            if (bluetooth.IsEnabled)
            {
                return bluetooth.Name;
            }
            else
            {
                sendMessage("Bluetooth is disabled");
                return null;
            }
        }

        void sendMessage(string message)
        {
            MessagingCenter.Send<IDevice, string>(this, "android error", message);
        }

        void sendInfoFrame(Stream writer, char sym)
        {
            string y = "#";
            byte[] output = Encoding.ASCII.GetBytes(y);
            byte[] output1 = { (byte)('#'), (byte)101, (byte)(sym) };
            //writer.Write(output);
            //writer.Write(frameId);
            //output = Encoding.ASCII.GetBytes(sym);
            //writer.Write(output);
            writer.Write(output1, 0, output1.Length);
            Console.WriteLine("Wysyłam");
        }

        void sendSpeedFrame(Stream writer, Point xy)
        {
            int frameId = 100;
            string y = "#";
            byte[] output = Encoding.ASCII.GetBytes(y);
            byte[] output1 = { (byte)('#'), (byte)100, (byte)((int)xy.X), (byte)((int)xy.Y) };
            //writer.Write(output);
            //writer.Write(frameId);
            //writer.Write(xy.X);
            //writer.Write(xy.Y);
            writer.Write(output1, 0, output1.Length);
            Console.WriteLine("Wysyłam");
        }


      
        public void connect(string mac) {
            try
            {
                BluetoothSocket socket = getSocket(mac);
                Stream output = socket.OutputStream;
                StreamWriter outputWriter = new StreamWriter(output);

                MessagingCenter.Subscribe<Page1, Point>(this, "xy", (sender, arg) =>
                {
                    sendSpeedFrame(output, arg);
                });

                MessagingCenter.Subscribe<Page1>(this, "run", (sender) =>
                {
                    sendInfoFrame(output, '1');
                });

                MessagingCenter.Subscribe<Page1>(this, "stop", (sender =>
                {
                    sendInfoFrame(output, '0');
                }));
                MessagingCenter.Send<IDevice>(this, "connected");
            }
            catch { Console.WriteLine("Connection failed entirely"); }
        }

        BluetoothSocket getSocket(string mac) {
            bluetooth = bluetoothCheck();
            BluetoothSocket socket = null;
            Console.WriteLine(mac);
            BluetoothDevice device = bluetooth.GetRemoteDevice(mac);

            try {
                socket = device.CreateInsecureRfcommSocketToServiceRecord(UUID.FromString(uudi));
            } catch {
                Console.WriteLine("unable to create socket");
            }

            try {
                socket.Connect();
                Console.WriteLine("Connection established");
            } catch{
                Console.WriteLine("Unable to connect");
                socket.Close();
            }
          
            return socket;
        }

      

        //stworzenie bluetooth adaptera i jego sprawdzenie
        private BluetoothAdapter bluetoothCheck()
        {
            BluetoothAdapter bluetooth1 = BluetoothAdapter.DefaultAdapter;
            if (bluetooth1 != null)
            {
                while(bluetooth1.IsEnabled)
                {
                    bluetooth1.Disable();
                }
               

                    try
                    {
                        while (!bluetooth1.IsEnabled)
                        {
                            bluetooth1.Enable();
                        }
                    }
                    catch
                    {
                        sendMessage("Failed to enable Bluetooth.Try again");
                        return null;
                    }
                }
                
            
            else
            {
                sendMessage("Bluetooth adapter - not found");
                return null;
            }

            bluetooth1.CancelDiscovery();

            return bluetooth1;
        }

        public void GetBlueList() {
            bluetooth = bluetoothCheck();
            bluetooth.CancelDiscovery();
            Console.WriteLine("Here we are");

            if (bluetooth.StartDiscovery())
            {
                MessagingCenter.Send<IDevice, string>(this, "android error", "discovery started");
                Console.WriteLine("Discovery started");
            }
            else {
                Console.WriteLine("Cant start discovery");
            }

            MessagingCenter.Subscribe<Page1>(this, "stop discovery", (sender) =>
            {
                MessagingCenter.Send<IDevice, string>(this, "android error", "discovery ended");
                bluetooth.CancelDiscovery();
                Console.WriteLine("Discovery ended");
                MessagingCenter.Unsubscribe<Page1>(this, "stop discovery");
            });
        
        }

       



    }

}
